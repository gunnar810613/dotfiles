filetype plugin indent on
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set smarttab
set shiftround
set autoindent
set smartindent

if isdirectory($HOME . "/tmp")
	set dir=~/tmp
	set undodir=~/tmp
	set backupdir=~/tmp
else
	set dir=/tmp
	set undodir=/tmp
	set backupdir=/tmp
endif

set swapfile
set undofile
set backup
set nowritebackup
set history=10000
set ruler
set number
set relativenumber
set lazyredraw
if ! has('nvim')
    set ttyfast
endif
set complete-=i
set display+=lastline
set encoding=utf-8
set filetype=unix
set wrap
set linebreak
set scrolloff=2
set sidescrolloff=5
set laststatus=2
set wildmenu
set tabpagemax=50
set noerrorbells
set novisualbell
set clipboard+=unnamedplus
set title
set go=
set mouse=
set nofoldenable
set autoread
set autowrite
set backspace=indent,eol,start
set confirm
set hidden
set formatoptions+=j
set nospell
set nolangremap
set shell=/bin/bash
set nrformats-=octal
set wildignore+=.swp
set nottimeout
set hlsearch
set incsearch
set splitbelow
set splitright
set nosmartcase
set ignorecase
set noshowmatch
set showmode
set wildmode=longest,list,full
set diffopt+=iwhite
