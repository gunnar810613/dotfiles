autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

autocmd BufReadPost *
            \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
            \ |   exe "normal! g`\""
            \ | endif
