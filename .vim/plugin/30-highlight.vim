syntax enable

let g:dracula_italic = 0
colors dracula

set termguicolors
set t_Co=256
set cursorline
