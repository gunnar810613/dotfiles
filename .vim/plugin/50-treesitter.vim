"treesitter
if has('nvim')
lua << EOF
    require'nvim-treesitter.configs'.setup {
        ensure_installed = { "java", "lua", "bash", "perl", "gitignore", "c", "vim" },
        sync_install = true,
        auto_install = true,
        highlight = {
            enable = true,
            additional_vim_regex_highlighting = false,
        },
    }
EOF
endif
