# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# PS1='\[$(tput bold; tput setaf 2)\]\w\$\[$(tput sgr0)\] '

HISTFILE=$HOME/.bash_history
HISTSIZE=16000
HISTFILESIZE=24000
shopt -s cmdhist histappend expand_aliases

# export EDITOR=vim
export EDITOR=nvim
export VISUAL=$EDITOR

export PAGER=less
export MANPAGER="sh -c 'col -bx | bat -l man -p'"

export BROWSER=firefox

alias ls='exa -al --icons --color=always --group-directories-first'
alias l=ls

# alias vi=vim
alias vim=nvim
alias vi=vim

alias rm='rm -Iv'
alias mv='mv -iv'
alias cp='cp -iv'

alias df='df -h'
alias du='du -h'
alias free='free -h'

alias dot='git --git-dir=$HOME/dotfiles --work-tree=$HOME -c status.showUntrackedFiles=no'

set -o vi
bind -m vi-command 'Control-l: clear-screen'
bind -m vi-insert 'Control-l: clear-screen'
stty -ixon

eval "$(dircolors -b)"
[[ -f /etc/profile.d/autojump.sh ]] && source /etc/profile.d/autojump.sh

if [[ -f /usr/share/bash-completion/bash_completion ]]; then
    source /usr/share/bash-completion/bash_completion
else 
    complete -cf sudo
fi
bind "set completion-ignore-case on"

eval "$(starship init bash)"
